package org.bannisters

import java.io.File

fun mainPart1() {
    val input = fileFromClasspath("input.txt").readText().trim()
    val machine = IntcodeMachine(input)
    machine.replace(1, 12)
    machine.replace(2, 2)
    
    machine.run()
    print(machine.valueAt(0))
}

fun main() {
    val input = fileFromClasspath("input.txt").readText().trim()
    val target = 19690720

    // parameters at 1 and 2 refer to memory positions so this is the highest they could be
    val highestIndex = input.filter { it == ',' }.count()
    
    0.rangeTo(highestIndex).forEach { limit ->
        // We'll start both numbers small and gradually increase
        0.rangeTo(limit).forEach { i ->
            val j = limit - i
            val machine = IntcodeMachine(input)
            machine.replace(1, i)
            machine.replace(2, j)
            machine.run()
            if (machine.valueAt(0) == target) {
                println("noun = $i, verb = $j")
                println("100 * $i + $j = ${100 * i + j}")
                return
            }
        }
    }
}

private fun fileFromClasspath(path: String) = File(object {}.javaClass.classLoader.getResource(path)!!.path)
