package org.bannisters

class IntcodeMachine(programText: String) {
    private var opcodes = programText.split(",").map(String::toInt)
    val programText get() = opcodes.joinToString(",")
    fun valueAt(position: Int) = opcodes[position]

    fun run(limit: Int = 10_000) {
        try {
            val process = IntcodeProcess(opcodes)
            0.rangeTo(limit).forEach { _ ->
                process.step()
            }
        } catch (halt: IntcodeProcess.Halt) {
            opcodes = halt.dump
        }
        return
    }

    fun replace(position: Int, newValue: Int) {
        opcodes = opcodes.mapIndexed { i, value -> if (i == position) newValue else value }
    }
}

class IntcodeProcess(intcodes: List<Int>) {
    internal constructor(vararg intcodes: Int) : this(intcodes.asList())

    private val state: MutableList<Int> = intcodes.toMutableList()
    internal var nextInstruction: Int = 0

    internal fun step() {
        val opcode = state[nextInstruction]
        when (opcode) {
            99 -> throw Halt(this.state)
            1 -> this.binaryOperator(Int::plus)
            2 -> this.binaryOperator(Int::times)
            else -> TODO("not yet implemented")
        }
    }

    private fun binaryOperator(op: (Int, Int) -> Int) {
        val ptr = nextInstruction
        val indexOfFirstValue = state[ptr + 1]
        val indexOfSecondValue = state[ptr + 2]
        val indexToStoreResult = state[ptr + 3]

        state[indexToStoreResult] = op(state[indexOfFirstValue], state[indexOfSecondValue])
        this.nextInstruction += 4
    }

    fun dumpCode() = state.joinToString(",")

    class Halt(val dump: List<Int>) : Exception()
}
