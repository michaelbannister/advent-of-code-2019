package org.bannisters

import ch.tutteli.atrium.api.fluent.en_GB.feature
import ch.tutteli.atrium.api.fluent.en_GB.toBe
import ch.tutteli.atrium.api.fluent.en_GB.toThrow
import ch.tutteli.atrium.api.verbs.expect
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class Day2Test {
    
    @Test
    fun `opcode 99 means halt`() {
        val program = IntcodeMachine("99")
        program.run()
        expect(program.programText).toBe("99")
    }
    
    @Test
    fun `opcode 99 means halt - step test`() {
        expect {
            IntcodeProcess(99).step()
        }.toThrow<IntcodeProcess.Halt>()
    }

    @Test
    fun `opcode 1 means add`() {
        val process = IntcodeProcess(1,4,5,6,2,3,0,99)
        process.step()
        expect(process)
                .feature({ f(it::dumpCode)}) { toBe("1,4,5,6,2,3,5,99") }
                .feature({ f(it::nextInstruction)}) { toBe(4) }
    }

    @Test
    fun `opcode 2 means multiply`() {
        val process = IntcodeProcess(2,4,5,6,2,3,0,99)
        process.step()
        expect(process)
                .feature({ f(it::dumpCode)}) { toBe("2,4,5,6,2,3,6,99") }
                .feature({ f(it::nextInstruction)}) { toBe(4) }
    }
    
    @Test
    fun `IntcodeMachine can replace a value in the program`() {
        val machine = IntcodeMachine("1")
        machine.replace(position=0, newValue=99)
        expect(machine.valueAt(0)).toBe(99)
    }
    
    @Test
    fun `IntcodeMachine can give a value at a specific position`() {
        val machine = IntcodeMachine("1,1,1,0,99")
        expect(machine.valueAt(0)).toBe(1)
        machine.run()
        expect(machine.valueAt(0)).toBe(2)
    }
}
