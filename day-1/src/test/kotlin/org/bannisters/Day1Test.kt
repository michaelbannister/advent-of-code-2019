package org.bannisters

import ch.tutteli.atrium.api.fluent.en_GB.toBe
import ch.tutteli.atrium.api.verbs.expect
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments.arguments
import org.junit.jupiter.params.provider.MethodSource

@TestInstance(PER_CLASS)
class Day1Test {

    @ParameterizedTest
    @MethodSource("fuelRequired")
    fun `calculate fuel required for a single module`(mass: Int, fuel: Int) {
        expect(fuelRequiredForMass(mass)).toBe(fuel)
    }

    @Test
    fun `fuel required for module taking into account the mass of the fuel`() {
        expect(totalFuelRequiredForModule(1969)).toBe(966)
    }
    
    @Test
    fun `calculate total fuel required`() {
        expect(
                calculateTotalFuelRequiredForModules(listOf(12, 1969))
        ).toBe(
                totalFuelRequiredForModule(12) + totalFuelRequiredForModule(1969)
        )
    }

    @Suppress("unused")
    fun fuelRequired() = listOf(
            arguments(12, 2),
            arguments(14, 2),
            arguments(1969, 654),
            arguments(100756, 33583)
    )

}
