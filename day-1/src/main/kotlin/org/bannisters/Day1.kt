package org.bannisters

import java.io.File

fun fuelRequiredForMass(mass: Int): Int {
    return (mass / 3) - 2
}

fun totalFuelRequiredForModule(moduleMass: Int) : Int {
    val initialFuelRequired = fuelRequiredForMass(moduleMass)
    val recursiveFuelMasses = generateSequence(initialFuelRequired) { fuelRequiredForMass(it) }
    return recursiveFuelMasses.takeWhile { it > 0 }.sum()
}

fun main(args: Array<String>) {
    val inputFile = fileFromClasspath("input.txt")
    val moduleMasses = inputFile.readLines(Charsets.UTF_8).mapNotNull { it.trim().toIntOrNull() }
    val totalFuel = calculateTotalFuelRequiredForModules(moduleMasses)
    println(totalFuel)
}

fun calculateTotalFuelRequiredForModules(moduleMasses: List<Int>): Int {
    val fuelRequirements = moduleMasses.map(::totalFuelRequiredForModule)
    return fuelRequirements.sum()
}

private fun fileFromClasspath(path: String) = File(object {}.javaClass.classLoader.getResource(path)!!.path)
