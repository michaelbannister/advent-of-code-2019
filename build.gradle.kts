import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.jetbrains.kotlin.jvm").version("1.3.61")
}

allprojects {
    plugins.apply("kotlin")
    repositories {
        jcenter()
    }
}
subprojects {
    dependencies {
        implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

        testImplementation("org.junit.jupiter:junit-jupiter-api:5.5.2")
        testImplementation("org.junit.jupiter:junit-jupiter-params:5.5.2")
        testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.5.2")

        testImplementation("ch.tutteli.atrium:atrium-fluent-en_GB:0.9.0-alpha")
    }

    tasks.withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "1.8"
    }
    tasks.named<Test>("test") {
        useJUnitPlatform()
    }
//    application {
//        mainClassName = "org.bannisters.AppKt"
//    }
}
